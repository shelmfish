# Makefile for HelpMessage
# Copyright © 2012-2014 Géraud Meyer <graud@gmx.com>
#
# This file is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License version 2 as published by the
# Free Software Foundation.
#
# This package is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this file.  If not, see <http://www.gnu.org/licenses/>.

# This a GNU Makefile.

prefix	?= $(HOME)/.local
bindir	?= $(prefix)/bin
datarootdir	?= $(prefix)/share
docdir	?= $(datarootdir)/doc/$(PACKAGE_TARNAME)
mandir	?= $(datarootdir)/man
# DESTDIR =  # distributors set this on the command line

empty	:=
tab	:= $(empty)	$(empty)

PACKAGE_NAME	?= HelpMessage
PACKAGE_TARNAME	?= helpmessage

# Get the version via git or from the VERSION file or from the project
# directory name.
VERSION	= $(shell test -x version.sh && ./version.sh $(PACKAGE_TARNAME) \
	  || echo "unknown_version")
# Allow either to be overwritten by setting DIST_VERSION on the command line.
ifdef DIST_VERSION
VERSION	= $(DIST_VERSION)
endif
PACKAGE_VERSION	= $(VERSION)

# Remove the _g<SHA1> part from the $VERSION
RPM_VERSION	= $(shell echo $(VERSION) | $(SED) -e 's/_g[0-9a-z]\+//')

PROGS	= 
SCRIPTS	= helpm2pod helpm2man helpm2text helpmselect gitparseopt2helpm helpm4sh
TESTS	= 
HELPMDOC	= $(HELPMSWITCH) $(HELPM4SHDOC)
HELPMSWITCH	= helpm2pod
HELPM4SHDOC	= helpm4sh gitparseopt2helpm helpmselect helpm2text helpm2man
HELPMESSAGE	= helpmessage.5.helpm
TEXTDOC	= $(HELPMDOC:%=%.1.txt) $(HELPMESSAGE:%.helpm=%.txt)
MANDOC	= $(HELPMDOC:%=%.1) $(HELPMESSAGE:%.helpm=%)
HTMLDOC	= $(HELPMDOC:%=%.1.html) $(HELPMESSAGE:%.helpm=%.html) README.html NEWS.html
RELEASEDOC = $(MANDOC) $(HTMLDOC)
ALLDOC	= $(TEXTDOC) $(MANDOC) $(HTMLDOC)

TARNAME	= $(PACKAGE_TARNAME)-$(RPM_VERSION)

RM	?= rm
CP	?= cp
MKDIR	?= mkdir
INSTALL	?= install
SED	?= sed
TAR	?= tar
TAR_FLAGS	= --owner root --group root --mode a+rX,o-w --mtime .
MAKE	?= make
ASCIIDOC	?= asciidoc
ASCIIDOC_FLAGS	= -apackagename="$(PACKAGE_NAME)" -aversion="$(VERSION)"
POD2MAN	?= pod2man
POD2MAN_FLAGS = --utf8 -c "HelpMessage Manuals" -r "$(PACKAGE_NAME) $(VERSION)"
POD2HTML	?= pod2html
POD2TEXT	?= pod2text
HELPM2POD	?= ./helpm2pod
HELPM4SH	?= ./helpm4sh
MD5SUM	?= md5sum
SHA512SUM	?= sha512sum

all: build
.help:
	@echo "Available targets for $(PACKAGE_NAME) Makefile:"
	@echo "	.help all configure build clean doc doc-txt doc-helpm doc-man doc-html"
	@echo "	dist install install-doc install-doc-man install-doc-html"
	@echo "Useful variables for $(PACKAGE_NAME) Makefile:"
	@echo "	prefix DESTDIR MAKE"
help: .help
.PHONY: .help help all build clean doc doc-txt doc-helpm doc-man doc-html \
	dist install install-doc install-doc-man install-doc-html

build: $(PROGS) $(TESTS)
doc: $(ALLDOC)
doc-txt: $(TEXTDOC)
doc-helpm: $(HELPMDOC:%=%.1.helpm)
doc-pod: $(HELPMDOC:%=%.pod)
doc-man: $(MANDOC)
doc-html: $(HTMLDOC)

test: $(TESTS) $(HELPMDOC:%=%.pod) $(HELPMESSAGE:%.5.helpm=%.pod)
	podchecker $(HELPMDOC:%=%.pod) $(HELPMESSAGE:%.5.helpm=%.pod)

install: build
	$(MKDIR) -p $(DESTDIR)$(bindir)
	set -e; for prog in $(PROGS) $(SCRIPTS); do \
		$(INSTALL) -p -m 0755 "$$prog" "$(DESTDIR)$(bindir)/$$prog"; \
	done
install-doc: install-doc-man install-doc-html
install-doc-man: doc-man
	$(MKDIR) -p $(DESTDIR)$(mandir)/man1
	set -e; for doc in $(MANDOC); do \
		gzip -9 <"$$doc" >"$$doc".gz; \
		$(INSTALL) -p -m 0644 "$$doc".gz "$(DESTDIR)$(mandir)/man1/"; \
		$(RM) "$$doc".gz; \
	done
install-doc-html: doc-html
	$(MKDIR) -p $(DESTDIR)$(docdir)
	set -e; for doc in $(HTMLDOC); do \
		$(INSTALL) -p -m 0644 "$$doc" "$(DESTDIR)$(docdir)/"; \
	done

clean:
	$(RM) -r $(TARNAME)
	$(RM) $(PACKAGE_TARNAME)-*.tar $(PACKAGE_TARNAME)-*.tar.gz \
	  $(PACKAGE_TARNAME)-*.tar.gz.md5 $(PACKAGE_TARNAME)-*.tar.gz.sha512
	$(RM) $(PROGS) $(TESTS)
	$(RM) *.xml pod2htm* $(HELPMDOC:%=%.1.helpm) \
	  $(HELPMDOC:%=%.pod) $(HELPMESSAGE:%.5.helpm=%.pod) *~ .*~
distclean: clean
	$(RM) ChangeLog $(ALLDOC)

$(TARNAME).tar : ChangeLog
	$(MKDIR) -p $(TARNAME)
	echo $(VERSION) > $(TARNAME)/VERSION
	$(CP) -p ChangeLog $(TARNAME)
	git archive --format=tar --prefix=$(TARNAME)/ HEAD > $(TARNAME).tar
	$(TAR) $(TAR_FLAGS) -rf $(TARNAME).tar $(TARNAME)
	$(RM) -r $(TARNAME)
dist : $(TARNAME).tar.gz
$(TARNAME).tar.gz : $(TARNAME).tar $(RELEASEDOC)
	$(MKDIR) $(TARNAME)
	$(CP) -p -P $(RELEASEDOC) $(TARNAME)
	$(TAR) $(TAR_FLAGS) -rf $(TARNAME).tar $(TARNAME)
	$(RM) -r $(TARNAME)
	gzip -f -9 $(TARNAME).tar
	$(MD5SUM) $(TARNAME).tar.gz > $(TARNAME).tar.gz.md5
	$(SHA512SUM) $(TARNAME).tar.gz > $(TARNAME).tar.gz.sha512
ChangeLog :
	( echo "# $@ for $(PACKAGE_NAME) - automatically generated from the VCS's history"; \
	  echo; \
	  ./gitchangelog.sh --tags --tag-pattern 'version\/[^\n]*' \
	    -- - --date-order --first-parent ) \
	| $(SED) 's/^\[version/\[version/' \
	> $@

README.html: README asciidoc.conf
	$(ASCIIDOC) $(ASCIIDOC_FLAGS) -b xhtml11 -d article -a readme $<
NEWS.html: NEWS asciidoc.conf
	$(ASCIIDOC) $(ASCIIDOC_FLAGS) -b xhtml11 -d article $<

$(HELPM4SHDOC:%=%.1.helpm): %.1.helpm: %
	$(HELPM4SH) helpm $< >$@
%.1.helpm: %
	./$< -h >$@
%.pod: %.5.helpm
	$(HELPM2POD) - <$< >$@
%.pod: %.1.helpm
	$(HELPM2POD) - <$< >$@

%.1: %.pod
	$(POD2MAN) $(POD2MAN_FLAGS) --section 1 $< >$@
%.5: %.pod
	$(POD2MAN) $(POD2MAN_FLAGS) --section 5 $< >$@
%.1.txt %.5.txt: %.pod
	$(POD2TEXT) --utf8 $< >$@
%.1.html %.5.html: %.pod
	$(POD2HTML) --noindex --title "$(shell printf "%s" "$(@:%.html=%)" | sed 's/\.\([0-9]\)$$/(\1)/' | tr a-z A-Z)" \
	  $< >$@
